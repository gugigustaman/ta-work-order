<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->string('id');
            $table->string('nama');
            $table->string('password');
            $table->string('area_id')->nullable();
            $table->enum('level', ['1', '2', '3', '4'])->comment('1 = Administrator, 2 = Team Leader, 3 = Help Desk, 4 = Teknisi TA');
            $table->rememberToken();
            $table->timestamps();
            $table->primary('id');

            $table->foreign('area_id')->references('id')->on('area')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
