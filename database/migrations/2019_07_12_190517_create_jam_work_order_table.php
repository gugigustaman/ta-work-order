<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJamWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jam_work_order', function (Blueprint $table) {
            $table->unsignedTinyInteger('jam');
            $table->boolean('khusus');
            $table->string('nama')->nullable();
            $table->timestamps();

            $table->primary('jam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jam_work_order');
    }
}
