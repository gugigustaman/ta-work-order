<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeknisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teknisi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('grup', ['TA', 'PA']);
            $table->string('kode')->nullable();
            $table->string('nama');
            $table->string('area_id')->nullable();
            $table->timestamps();

            $table->foreign('area_id')->references('id')->on('area')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teknisi');
    }
}
