<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeknisiHarianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teknisi_harian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->unsignedInteger('ta')->default(0);
            $table->unsignedInteger('pa')->default(0);
            $table->string('sto_id')->nullable();
            $table->string('reporter_id')->nullable();
            $table->timestamps();

            $table->foreign('sto_id')->references('id')->on('sto')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('reporter_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teknisi_harian');
    }
}
