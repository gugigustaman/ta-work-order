<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkOrderLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_order_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('work_order_id');
            $table->unsignedTinyInteger('jam')->nullable();
            $table->string('status')->default('ORDER');
            $table->string('kendala')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('reporter_id')->nullable();
            $table->timestamps();

            $table->foreign('work_order_id')->references('id')->on('work_order')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('jam')->references('jam')->on('jam_work_order')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('status')->references('id')->on('status_work_order')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('kendala')->references('id')->on('kendala')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('reporter_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_order_log');
    }
}
