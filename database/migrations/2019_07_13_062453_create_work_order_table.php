<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jenis_work_order_id')->nullable();
            $table->string('sto_id')->nullable();
            $table->string('no_speedy')->nullable();
            $table->string('no_sc')->nullable();
            $table->unsignedTinyInteger('jam')->nullable();
            $table->string('status')->default('ORDER');
            $table->string('kendala')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('reporter_id')->nullable();
            $table->timestamps();

            $table->foreign('jenis_work_order_id')->references('id')->on('jenis_work_order')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('sto_id')->references('id')->on('sto')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('jam')->references('jam')->on('jam_work_order')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('status')->references('id')->on('status_work_order')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('kendala')->references('id')->on('kendala')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('reporter_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_order');
    }
}
