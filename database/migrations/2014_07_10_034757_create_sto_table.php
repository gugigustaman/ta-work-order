<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sto', function (Blueprint $table) {
            $table->string('id');
            $table->string('datel_id')->nullable();
            $table->string('area_id')->nullable();
            $table->string('nama');
            $table->timestamps();
            $table->primary('id');

            $table->foreign('datel_id')->references('id')->on('datel')->onUpdate('cascade')->onDelete('set null');

            $table->foreign('area_id')->references('id')->on('area')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sto');
    }
}
