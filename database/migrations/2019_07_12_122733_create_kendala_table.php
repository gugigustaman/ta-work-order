<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKendalaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendala', function (Blueprint $table) {
            $table->string('id');
            $table->string('status_work_order_id')->nullable();
            $table->string('deskripsi')->nullbale();
            $table->timestamps();
            $table->primary('id');

            $table->foreign('status_work_order_id')->references('id')->on('status_work_order')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendala');
    }
}
