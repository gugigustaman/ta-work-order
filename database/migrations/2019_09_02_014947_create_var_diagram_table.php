<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVarDiagramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('var_diagram', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('ps_3p')->default(0);
            $table->unsignedInteger('ps_2p')->default(0);
            $table->unsignedInteger('ps_non_ms2n')->default(0);
            $table->unsignedInteger('live_rwos')->default(0);
            $table->unsignedInteger('live_actcomp')->default(0);
            $table->unsignedInteger('live_fallout')->default(0);
            $table->unsignedInteger('fallout')->default(0);
            $table->unsignedInteger('fallout_act')->default(0);
            $table->unsignedInteger('fallout_data')->default(0);
            $table->unsignedInteger('fallout_wfm')->default(0);
            $table->unsignedInteger('fallout_osm')->default(0);
            $table->unsignedInteger('mandeg')->default(0);
            $table->string('reporter_id')->nullable();
            $table->timestamps();

            $table->foreign('reporter_id')->references('id')->on('user')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('var_diagram');
    }
}
