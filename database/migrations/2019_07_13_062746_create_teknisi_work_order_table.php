<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeknisiWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teknisi_work_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('work_order_id')->unsigned();
            $table->bigInteger('teknisi_id')->unsigned();
            $table->timestamps();

            $table->foreign('work_order_id')->references('id')->on('work_order')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('teknisi_id')->references('id')->on('teknisi')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teknisi_work_order');
    }
}
