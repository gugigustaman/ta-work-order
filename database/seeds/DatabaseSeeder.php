<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DatelTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(StoTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(JenisWorkOrderTableSeeder::class);
        $this->call(StatusWorkOrderTableSeeder::class);
        $this->call(KendalaTableSeeder::class);
        $this->call(TeknisiTableSeeder::class);
        $this->call(JamWorkOrderTableSeeder::class);
    }
}
