<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class JamWorkOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jam_work_order')->insert([
            [
            	'jam' => 8,
            	'khusus' => 1,
            	'nama' => 'PAGI',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'jam' => 10,
            	'khusus' => 0,
            	'nama' => null,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'jam' => 12,
            	'khusus' => 0,
            	'nama' => null,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'jam' => 15,
            	'khusus' => 0,
            	'nama' => null,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'jam' => 17,
            	'khusus' => 0,
            	'nama' => null,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'jam' => 19,
            	'khusus' => 0,
            	'nama' => null,
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ]
        ]);
    }
}
