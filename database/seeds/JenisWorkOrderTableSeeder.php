<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class JenisWorkOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_work_order')->insert([
            [
            	'id' => '1P',
            	'nama' => 'Internet Only / VoIP Only / TV Only',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => '2P',
            	'nama' => 'Internet + VoIp / Internet + TV',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => '3P',
            	'nama' => 'Internet + VoIP + TV',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'ADDSTB',
            	'nama' => 'Tambah STB',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
                'id' => 'UPSELLING',
                'nama' => 'Upselling',
                'created_at' => Carbon::now(),
                'updated_at' => null
            ],
        ]);
    }
}
