<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('area')->insert([
            [
            	'id' => 'BDT1',
            	'nama' => 'Bandung Timur 1',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'BDT2',
            	'nama' => 'Bandung Timur 2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'BDT3',
            	'nama' => 'Bandung Timur 3',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'BDT4',
            	'nama' => 'Bandung Timur 4',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'BDB3',
            	'nama' => 'Bandung Barat 3',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'BDB4',
            	'nama' => 'Bandung Barat 4',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
        ]);
    }
}
