<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('datel')->insert([
            [
            	'id' => 'HS1',
            	'nama' => 'Home Service 1',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'HS2',
            	'nama' => 'Home Service 2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'HS3',
            	'nama' => 'Home Service 3',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'SMD',
            	'nama' => 'Sumedang',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ]
        ]);
    }
}
