<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'id' => '95153416',
            'nama' => 'Nurrahman',
            'password' => bcrypt('rahasia'),
            'area_id' => 'BDT2',
            'level' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => null
        ]);
    }
}
