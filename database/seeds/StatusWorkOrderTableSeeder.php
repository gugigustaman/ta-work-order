<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusWorkOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_work_order')->insert([
            [
            	'id' => 'ORDER',
            	'deskripsi' => 'On Progress',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'MANJA',
            	'deskripsi' => 'Manajemen Janji',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'CEKLOK',
            	'deskripsi' => 'Cek Lokasi',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'TARIK DC',
            	'deskripsi' => 'Tarik Drop Core',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'ONPROGRESS SETTING',
            	'deskripsi' => 'Onprogress Setting',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'LIVE',
            	'deskripsi' => 'Live Belum PS',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'PS',
            	'deskripsi' => 'Selesai',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'DOUBLE INPUT',
            	'deskripsi' => 'Double Input',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'KENDALA PELANGGAN',
            	'deskripsi' => 'Kendala Pelanggan',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
            [
            	'id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Kendala Teknis',
            	'created_at' => Carbon::now(),
            	'updated_at' => null,
            ],
        ]);
    }
}
