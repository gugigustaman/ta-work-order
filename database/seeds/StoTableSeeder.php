<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sto')->insert([
            [
            	'id' => 'DGO',
            	'area_id' => 'BDT1',
            	'datel_id' => 'HS2',
            	'nama' => 'Dago',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'ANI',
            	'area_id' => 'BDT1',
            	'datel_id' => 'HS1',
            	'nama' => 'Antapani',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'LBG',
            	'area_id' => 'BDT2',
            	'datel_id' => 'HS1',
            	'nama' => 'Lembong',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'TRG',
            	'area_id' => 'BDT2',
            	'datel_id' => 'HS3',
            	'nama' => 'Turangga',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'CJA',
            	'area_id' => 'BDT3',
            	'datel_id' => 'SMD',
            	'nama' => 'Cijaura',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'UBR',
            	'area_id' => 'BDT4',
            	'datel_id' => 'SMD',
            	'nama' => 'Ujung Berung',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'SMD',
            	'area_id' => 'BDT4',
            	'datel_id' => 'SMD',
            	'nama' => 'Sumedang',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'TAS',
            	'area_id' => 'BDT4',
            	'datel_id' => 'SMD',
            	'nama' => 'Tanjung Sari',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'HGM',
            	'area_id' => 'BDB3',
            	'datel_id' => 'HS2',
            	'nama' => 'Hegarmanah',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'GGK',
            	'area_id' => 'BDB3',
            	'datel_id' => 'HS2',
            	'nama' => 'Geger Kalong',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'KPO',
            	'area_id' => 'BDB4',
            	'datel_id' => 'HS3',
            	'nama' => 'Kopo',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'id' => 'TLE',
            	'area_id' => 'BDB4',
            	'datel_id' => 'HS3',
            	'nama' => 'Tegal Lega',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
        ]);
    }
}
