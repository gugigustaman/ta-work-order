<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TeknisiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teknisi')->insert([
            [
            	'grup' => 'TA',
            	'kode' => '97156189',
            	'nama' => 'ROMI',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'grup' => 'TA',
            	'kode' => '96150375',
            	'nama' => 'YOPPY',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'grup' => 'TA',
            	'kode' => '97150329',
            	'nama' => 'ADNAN',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'grup' => 'TA',
            	'kode' => '18990054',
            	'nama' => 'FARID',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'grup' => 'TA',
            	'kode' => '97156182',
            	'nama' => 'FAUZAN',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'grup' => 'TA',
            	'kode' => '97159160',
            	'nama' => 'HARDI',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
            [
            	'grup' => 'TA',
            	'kode' => '98160147',
            	'nama' => 'AZIZ',
            	'area_id' => 'BDT2',
            	'created_at' => Carbon::now(),
            	'updated_at' => null
            ],
        ]);
    }
}
