<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KendalaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kendala')->insert([
            [
            	'id' => 'Kendala Perijinan',
            	'status_work_order_id' => 'KENDALA PELANGGAN',
            	'deskripsi' => 'Kendala Perijinan',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Odp Blm Golive',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Odp Blm Golive',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Ca+Inpul',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Ca+Inpul',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Salah Area/Lanjut Dispatch Area Lain',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Salah Area/Lanjut Dispatch Area Lain',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'ODP Full',
            	'v' => 'KENDALA TEKNIS',
            	'deskripsi' => 'ODP Full',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'ODP Loss',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'ODP Loss',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Jaringan Tdk Layak',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Jaringan Tdk Layak',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Tidak Ada Jaringan',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Tidak Ada Jaringan',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Reschedule',
            	'status_work_order_id' => 'KENDALA PELANGGAN',
            	'deskripsi' => 'Reschedule',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Alamat Tidak Ketemu',
            	'status_work_order_id' => 'KENDALA PELANGGAN',
            	'deskripsi' => 'Alamat Tidak Ketemu',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Rukos',
            	'status_work_order_id' => 'KENDALA PELANGGAN',
            	'deskripsi' =>'Rukos',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Pelanggan Minta Batal/Tdk Minat',
            	'status_work_order_id' => 'KENDALA PELANGGAN',
            	'deskripsi' => 'Pelanggan Minta Batal/Tdk Minat',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Tunggu Konfirmasi Pelanggan',
            	'status_work_order_id' => 'KENDALA PELANGGAN',
            	'deskripsi' => 'Tunggu Konfirmasi Pelanggan',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Sudah Terpasang di Nomor Lain/Double Input',
            	'status_work_order_id' => 'DOUBLE INPUT',
            	'deskripsi' => 'Sudah Terpasang di Nomor Lain/Double Input',
            	'created_at' => Carbon::now()
        	],
            [
            	'id' => 'Revoke',
            	'status_work_order_id' => 'KENDALA TEKNIS',
            	'deskripsi' => 'Revoke',
            	'created_at' => Carbon::now()
        	]
        ]);
    }
}
