@extends('layouts.app')
@section('title', 'Daftar Work Order')

@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Input Work Order</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/work_order">Work Order</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Input</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form class="card" method="POST" action="/work_order">
                    @csrf()
                    <div class="card-body">
                        @if (session()->has('message'))
                        <div class="alert alert-{{ session('type') }} mx-4" user="alert">
                            {!! session('message') !!}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label for="orders">Work Order (Satu Work Order per Baris)</label>
                                    <textarea type="text" rows="20" class="form-control" id="orders" name="orders" placeholder="Input satu atau lebih work order disini..." required></textarea>
                                    <small id="emailHelp2" class="form-text text-muted">Format : JAM_INPUT;STO;JENIS_WO;NO_SC;NO_SPEEDY;TA/PA|KODE_TEKNISI_1,TA/PA|KODE_TEKNISI_2;STATUS;KENDALA;KETERANGAN_KENDALA
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <a href="/work_order" class="btn btn-danger">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection