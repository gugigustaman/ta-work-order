@extends('layouts.app')
@section('title', 'Daftar Work Order')

@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">@yield('title') &nbsp; <a href="/work_order/create" class="btn btn-sm btn-danger"><i class="fa fa-plus"></i></a></h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/work_order">Work Order</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if (session()->has('message'))
                        <div class="alert alert-{{ session('type') }} mx-4" role="alert">
                            {!! session('message') !!}
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table id="table" class="display table table-striped table-hover" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis WO</th>
                                        <th>STO</th>
                                        <th>No. SC</th>
                                        <th>No. Speedy</th>
                                        <th>Jam</th>
                                        <th>Status</th>
                                        <th>Pelapor</th>
                                        <!-- <th>Aksi</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="POST" id="deleteForm">
    @csrf()
    @method('DELETE')
</form>
@endsection

@section('scripts')
<script src="/assets/js/plugin/datatables/datatables.min.js"></script>
<script >
    $(function() {
        var indexAjax = {
            url: '/work_order/dtIndex',
            contentType: "application/json",
            type: "GET",
            data: function ( d ) {
                return d;
            }
        };

        var table = $('#table').DataTable({
            // stateSave: true,
            processing: true,
            serverSide: true,
            searching: true,
            // responsive: true,
            ajax: indexAjax,
            aaSorting: [[5,'asc']],
            language: {
              paginate: {
                next: '<i class="fas fa-angle-right"></i>',
                previous: '<i class="fas fa-angle-left"></i>'
              }
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', responsivePriority: 1, searchable: false },
                { data: 'jenis_work_order_id', name: 'jenis_work_order_id', responsivePriority: 1 },
                { data: 'sto_id', name: 'sto_id', responsivePriority: 1 },
                { data: 'no_sc', name: 'no_sc', responsivePriority: 1 },
                { data: 'no_speedy', name: 'no_speedy', responsivePriority: 1 },
                { data: 'jam', name: 'jam', responsivePriority: 1 },
                { data: 'status', name: 'status', responsivePriority: 1 },
                { data: 'reporter.nama', name: 'reporter.nama', responsivePriority: 1 },
                // { data: 'id', name: 'id', className: 'text-center', responsivePriority: 1, render: function(data, type, row) {
                //     var buttons = '';
                //     buttons += '<a href="/work_order/'+data+'/edit" class="btn btn-sm btn-custon-four btn-warning"><i class="fa fa-edit"></i></a> ';
                //     return buttons +
                //         '<button type="button" class="btn btn-sm btn-danger delete" data-id="'+data+'" data-toggle="modal" data-target="#resetModalAlert"><i class="fa fa-trash"></i></button>';
                // }},
            ]
        });

        $('#multi-filter-select').DataTable( {
            "pageLength": 5,
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                            );

                        column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                    } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });

        $('body').on('click', '.delete', function(e) {
            $('#deleteForm').attr('action', '/work_order/'+$(this).data('id'));
            swal({
                title: 'Apakah Anda yakin?',
                text: "Work Order tersebut akan dihapus!",
                type: 'warning',
                buttons:{
                    confirm: {
                        text : 'Ya, hapus!',
                        className : 'btn btn-danger'
                    },
                    cancel: {
                        visible: true,
                        text: 'Batal',
                        className: 'btn btn-default'
                    }
                }
            }).then((Delete) => {
                $('#deleteForm').submit();
            });
        });
    });
</script>
@endsection