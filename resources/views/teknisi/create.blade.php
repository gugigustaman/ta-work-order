@extends('layouts.app')
@section('title', 'Daftar Teknisi')

@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Tambah Teknisi</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/teknisi">Teknisi</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Tambah</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form class="card" method="POST" action="/teknisi">
                    @csrf()
                    <div class="card-body">
                        @if (session()->has('message'))
                        <div class="alert alert-{{ session('type') }} mx-4" teknisi="alert">
                            {!! session('message') !!}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
                                    <!-- <small id="emailHelp2" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group">
                                    <label for="kode">Kode</label>
                                    <input type="kode" class="form-control" id="kode" name="kode" placeholder="Kode" required>
                                    <!-- <small id="emailHelp2" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">             
                                <div class="form-group">
                                    <label for="grup">Grup</label>
                                    <select class="form-control" id="grup" name="grup" required>
                                        <option value="">- Pilih Grup -</option>
                                        <option value="TA">Telkom Akses</option>
                                        <option value="PA">Partner Akses</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="area">Area</label>
                                    <select class="form-control" id="area" name="area" required>
                                        <option value="">- Pilih Grup Area -</option>
                                        @foreach ($areas as $a)
                                        <option value="{{ $a->id }}">{{ $a->id . ' - ' . $a->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <a href="/teknisi" class="btn btn-danger">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection