<html lang="en" class="wf-lato-n4-active wf-flaticon-n4-inactive wf-simplelineicons-n4-active wf-lato-n7-active wf-lato-n3-active wf-lato-n9-active wf-fontawesome5solid-n4-active wf-fontawesome5regular-n4-active wf-fontawesome5brands-n4-active wf-active">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | Telkom Akses Work Order</title>
    <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/img/favicon.png">

    <!-- Fonts and icons -->
    <script src="/assets/js/plugin/webfont/webfont.min.js"></script>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" media="all"><link rel="stylesheet" href="/assets/css/fonts.min.css" media="all">
    <script>
        WebFont.load({
            google: {"families":["Lato:300,400,700,900"]},
            custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['/assets/css/fonts.min.css']},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    
    <!-- CSS Files -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/atlantis.css">

    <style type="text/css">
        .login {
            background: #efefee;
        }
        .login .wrapper.wrapper-login {
            display: flex;
            justify-content: center;
            align-items: center;
            height: unset;
            padding: 15px;
        }
        .login .wrapper.wrapper-login .container-login:not(.container-transparent), .login .wrapper.wrapper-login .container-signup:not(.container-transparent) {
            background: #fff;
            -webkit-box-shadow: 0 0.75rem 1.5rem rgba(18,38,63,.03);
            -moz-box-shadow: 0 .75rem 1.5rem rgba(18,38,63,.03);
            box-shadow: 0 0.75rem 1.5rem rgba(18,38,63,.03);
            border: 1px solid #ebecec;
        }
        .login .wrapper.wrapper-login .container-login, .login .wrapper.wrapper-login .container-signup {
            width: 400px;
            padding: 25px 25px;
            border-radius: 5px;
        }
        .login .wrapper.wrapper-login .container-login h3, .login .wrapper.wrapper-login .container-signup h3 {
            font-size: 19px;
            font-weight: 600;
            margin-bottom: 25px;
        }
        .login .show-password {
            position: absolute;
            right: 20px;
            top: 50%;
            transform: translateY(-50%);
            font-size: 20px;
            cursor: pointer;
        }
        .login .wrapper.wrapper-login .container-login .form-sub, .login .wrapper.wrapper-login .container-signup .form-sub {
            align-items: center;
            justify-content: space-between;
            padding: 8px 10px;
        }
        .login .wrapper.wrapper-login .container-login .form-action, .login .wrapper.wrapper-login .container-signup .form-action {
            text-align: center;
            padding: 25px 10px 0;
        }
        .login .wrapper.wrapper-login .container-login .login-account, .login .wrapper.wrapper-login .container-signup .login-account {
            padding-top: 10px;
            text-align: center;
        }
    </style>
</head>
<body class="login" style="" cz-shortcut-listen="true">
    <div class="wrapper wrapper-login">
        <div class="container container-login animated fadeIn" style="display: block;">
            <img src="/assets/img/logo_ta.png" class="img-fluid mb-2" />
            <!-- <h3 class="text-center">Work Order</h3> -->
            <form class="login-form" action="/login" method="POST">
                @csrf()
                <div class="form-group form-floating-label">
                    <input id="id" name="id" type="text" class="form-control input-border-bottom" required="">
                    <label for="id" class="placeholder">Username</label>
                </div>
                <div class="form-group form-floating-label">
                    <input id="password" name="password" type="password" class="form-control input-border-bottom" required="">
                    <label for="password" class="placeholder">Password</label>
                    <div class="show-password">
                        <i class="icon-eye"></i>
                    </div>
                </div>
                <!-- <div class="row form-sub m-0">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="rememberme">
                        <label class="custom-control-label" for="rememberme">Remember Me</label>
                    </div>
                    
                    <a href="#" class="link float-right">Forget Password ?</a>
                </div> -->
                <div class="form-action">
                    <button type="submit" class="btn btn-danger btn-rounded btn-login">Masuk</button>
                </div>
                <!-- <div class="login-account">
                    <span class="msg">Don't have an account yet ?</span>
                    <a href="#" id="show-signup" class="link">Sign Up</a>
                </div> -->
            </form>
        </div>
    </div>
    <script src="/assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="/assets/js/core/popper.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>
    <script src="/assets/js/atlantis.min.js"></script>
<script type="text/javascript"></script>
<style>
    .tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}
    .tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}
    .ws_toolbar {z-index:100000} 
    .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   
    .tb_highlight{background-color:yellow} 
    .tb_hide {visibility:hidden} 
    .ws_toolbar img {padding:2px;margin:0px}
</style></body></html>