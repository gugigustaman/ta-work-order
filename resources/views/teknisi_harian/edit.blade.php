@extends('layouts.app')
@section('title', 'Daftar Data Teknisi Harian')

@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Ubah Data Teknisi Harian</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/teknisi_harian">Data Teknisi Harian</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Ubah</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form class="card" method="POST" action="/teknisi_harian/{{ $teknisi_harian->id }}">
                    @csrf()
                    @method('PATCH')
                    <div class="card-body">
                        @if (session()->has('message'))
                        <div class="alert alert-{{ session('type') }} mx-4" user="alert">
                            {!! session('message') !!}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="tanggal">Tanggal</label>
                                    <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" required value="{{ $teknisi_harian->tanggal }}" max="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                    <!-- <small id="emailHelp2" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group">
                                    <label for="sto">STO</label>
                                    <select class="form-control" id="sto" name="sto" required>
                                        <option value="">- Pilih STO -</option>
                                        @foreach ($sto as $s)
                                        <option value="{{ $s->id }}"
                                            @if ($s->id == $teknisi_harian->sto_id)
                                             selected 
                                            @endif
                                            >{{ $s->id . ' - ' . $s->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">             
                                <div class="form-group">
                                    <label for="ta">Jumlah Teknisi TA</label>
                                    <input type="number" class="form-control" id="ta" name="ta" placeholder="Jumlah Teknisi TA" required value="{{ $teknisi_harian->ta }}" min="0">
                                </div>
                                <div class="form-group">
                                    <label for="pa">Jumlah Teknisi PA</label>
                                    <input type="number" class="form-control" id="pa" name="pa" placeholder="Jumlah Teknisi PA" required value="{{ $teknisi_harian->pa }}" min="0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <a href="/teknisi_harian" class="btn btn-danger">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection