@extends('layouts.app')
@section('title', 'Ubah Pengguna')

@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">@yield('title')</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/user">Pengguna</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Ubah</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form class="card" method="POST" action="/user/{{ $user->id }}">
                    @csrf()
                    @method('PATCH')
                    <div class="card-body">
                        @if (session()->has('message'))
                        <div class="alert alert-{{ session('type') }} mx-4" user="alert">
                            {!! session('message') !!}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="nama" class="form-control" id="nama" name="nama" placeholder="Nama" required value="{{ $user->name }}">
                                    <!-- <small id="emailHelp2" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                </div>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" required value="{{ $user->username }}">
                                </div>
                                <div class="form-group">
                                    <label for="area">Grup Area</label>
                                    <select class="form-control" id="area" name="area" required>
                                        <option value="">- Pilih Grup Area -</option>
                                        @foreach ($areagroups as $ag)
                                        <option value="{{ $ag->id }}"
                                        @if ($ag->id == $user->area_group_id) selected @endif >{{ $ag->id . ' - ' . $ag->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">             
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="password">Konfirmasi Password</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Konfirmasi Password">
                                </div>
                                <div class="form-group">
                                    <label for="level">Level</label>
                                    <select class="form-control" id="level" name="level" required>
                                        <option value="">- Pilih Level -</option>
                                        @foreach ($userlevels as $ul)
                                        <option value="{{ $ul['id'] }}" @if ($ul['id'] == $user->level) selected @endif>{{ $ul['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <a href="/user" class="btn btn-danger">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection