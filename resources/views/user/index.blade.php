@extends('layouts.app')
@section('title', 'Daftar Pengguna')

@section('content')
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">@yield('title') &nbsp; <a href="/user/create" class="btn btn-sm btn-danger"><i class="fa fa-plus"></i></a></h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="/">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="/user">Pengguna</a>
                </li>
                <!-- <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Basic Form</a>
                </li> -->
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if (session()->has('message'))
                        <div class="alert alert-{{ session('type') }} mx-4" role="alert">
                            {!! session('message') !!}
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table id="table" class="display table table-striped table-hover" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Grup Area</th>
                                        <th>Level</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="POST" id="deleteForm">
    @csrf()
    @method('DELETE')
</form>
@endsection

@section('scripts')
<script src="/assets/js/plugin/sweetalert/sweetalert.min.js"></script>
<script src="/assets/js/plugin/datatables/datatables.min.js"></script>
<script >
    $(function() {
        var indexAjax = {
            url: '/user/dtIndex',
            contentType: "application/json",
            type: "GET",
            data: function ( d ) {
                return d;
            }
        };

        var table = $('#table').DataTable({
            // stateSave: true,
            processing: true,
            serverSide: true,
            searching: true,
            // responsive: true,
            ajax: indexAjax,
            aaSorting: [[5,'asc']],
            language: {
              paginate: {
                next: '<i class="fas fa-angle-right"></i>',
                previous: '<i class="fas fa-angle-left"></i>'
              }
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', responsivePriority: 1, searchable: false },
                { data: 'nama', name: 'nama', responsivePriority: 1 },
                { data: 'id', name: 'id', responsivePriority: 1 },
                { data: 'area_id', name: 'area_id', responsivePriority: 1 },
                { data: 'level', name: 'level', responsivePriority: 1, className: 'text-center', render: function(data, type, row) {
                    switch(data) {
                        case '1': return 'Administrator';
                        case '2': return 'Team Leader';
                        case '3': return 'Help Desk';
                        case '4': return 'Teknisi TA';
                        default: return 'N/A';
                    }
                }},
                { data: 'id', name: 'id', className: 'text-center', responsivePriority: 1, render: function(data, type, row) {
                    var buttons = '';
                    buttons += '<a href="/user/'+data+'/edit" class="btn btn-sm btn-custon-four btn-warning"><i class="fa fa-edit"></i></a> ';
                    return buttons +
                        '<button type="button" class="btn btn-sm btn-danger delete" data-id="'+data+'" data-toggle="modal" data-target="#resetModalAlert"><i class="fa fa-trash"></i></button>';
                }},
            ]
        });

        $('#multi-filter-select').DataTable( {
            "pageLength": 5,
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                            );

                        column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                    } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function() {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
                ]);
            $('#addRowModal').modal('hide');

        });

        $('body').on('click', '.delete', function(e) {
            $('#deleteForm').attr('action', '/user/'+$(this).data('id'));
            swal({
                title: 'Apakah Anda yakin?',
                text: "Pengguna tersebut akan dihapus!",
                type: 'warning',
                buttons:{
                    confirm: {
                        text : 'Ya, hapus!',
                        className : 'btn btn-danger'
                    },
                    cancel: {
                        visible: true,
                        text: 'Batal',
                        className: 'btn btn-default'
                    }
                }
            }).then((Delete) => {
                if (Delete) $('#deleteForm').submit();
            });
        });
    });
</script>
@endsection