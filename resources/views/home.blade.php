@extends('layouts.app')
@section('title', 'Home')

@section('styles')
<link rel="stylesheet" href="/assets/vendor/treantjs/Treant.css">
<link rel="stylesheet" href="/assets/vendor/treantjs/example7.css">

<style type="text/css">
    .diagram-table {
        border: 1px solid #000;
        color: #000;
    }

    .diagram-table td {
        text-align: center;
        padding: 2px;
        border: 1px solid #000;   
        font-size: 10px;
        background: #fff;
    }

    .diagram-table tr:first-child td {
        font-weight: bold;
    }

    table.thirdlevel {
        width: 140px;
        table-layout: fixed;
    }

    table.wo tr:first-child td {
        background: #00B0F0;
    }

    table.p2msdn, table.nonps {
        width: 100px;
        table-layout: fixed;
    }

    table.p2msdn tr:first-child td {
        background: #00FF00;
    }

    table.nonps tr:first-child td {
        background: #FFCC00;
    }

    table.kendala tr:first-child td {
        background: #FFFF00;
    }

    table.progresswo tr:first-child td {
        background: #F8CBAD;
    }

    table.description {
        width: 230px;
    }

    table.description tr:first-child td {
        font-weight: normal;
    }

    table.description tr td:first-child {
        text-align: left;
        background: #FFFF00;
    }

    table.description tr td:nth-child(2) {
        width: 45px !important;
    }

    table.description tr td.exist
    {
        width: 45px;
        background: #FFC000;
    }

    table.description tr:nth-child(6) td:first-child {
        text-align: center;
        background: #FFFFFF;
    }

    table.description2 {
        width: 230px;
    }

    table.description2 tr:first-child td {
        font-weight: normal;
    }

    table.description2 tr td:nth-child(2),
    table.description2 tr td:nth-child(3) {
        width: 45px;
    }

    table.description2 tr td:first-child {
        text-align: left;
        background: #F8CBAD;
    }

    table.description2 tr:nth-child(9) td {
        font-weight: bold;
    }

    table.description2 tr:nth-child(9) td:first-child {
        text-align: center;
        background: #FFFFFF;
    }

    table.sc-table {
        width: 250px;
    }

    table.sc-table tr td:first-child {
        text-align: left;
        padding-left: 20px;
    }

    table.sc-table tr td:last-child {
        text-align: center !important;
        padding: 2px !important;
        width: 70px;
    }

    table.sc-table tr:first-child td {
        background: #5B9BD5;
        color: #fff;
        font-weight: bold;
        text-align: left;
        padding-left: 0px !important;
    }

    table.sc-table tr.total td:first-child {
        padding-left: 2px !important;
        font-weight: bold;
    }

    table.sc-table tr.parent td
    {
        background: #BDD7EE;
        text-align: left;
        padding-left: 10px;
        font-weight: bold;
    }

    table.fifthlevel {
        margin-bottom: 15px;
        width: 250px;
    }

    table.fifthlevel tr td {
        font-weight: normal !important;
    }

    table.fifthlevel tr td:first-child {
        text-align: left;
    }

    table.fifthlevel tr td:last-child {
        text-align: center !important;
        padding: 2px !important;
        width: 70px;
    }

    table.fallout tr td:first-child {
        background: #FFFF00;
    }

    #summaryTable {
        border: 1px solid #000 !important;
    }

    #summaryTable tr th {
        text-align: center;
        font-weight: bold;
    }

    #summaryTable tr th, #summaryTable tr td {
        border: 1px solid #000 !important;
        font-size: 11px !important;
        padding: 2px !important;
        height: auto !important;
        text-align: center;
    }

    #summaryTable tr td:first-child {
        text-align: center;
    }

    #summaryTable tr td:first-child {
        text-align: center;
    }

    #summaryTable tr:last-child td {
        font-weight: bold;
    }

    .tennis-draw {
        width: auto !important;
    }

    .editable {
        position: relative;
    }

    .editable input {
        text-align: center;
        width: 100%;
        padding-left: 0px;
    }

    .editable input:read-only {
        background: transparent;
        color: #000;
        border: 1px solid transparent !important;
    }

    .editable input:read-write {
        background: transparent;
        color: #000;
        border: 1px solid red !important;
    }

    .editbtn {
        position: absolute;
        right: 5px;
        color: #276700;
        text-decoration: underline;
    }
</style>
@endsection

@section('content')
<div class="content">
    <div class="panel-header bg-danger-gradient">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                    <!-- <h5 class="text-white op-7 mb-2">Free Bootstrap 4 Admin Dashboard</h5> -->
                </div>
                <!-- <div class="ml-md-auto py-2 py-md-0">
                    <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
                    <a href="#" class="btn btn-secondary btn-round">Add Customer</a>
                </div> -->
            </div>
        </div>
    </div>
    <div class="page-inner mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-tools" style="float: right;">
                            <button class="btn btn-danger btn-sm editBtn"><span class="fa fa-edit"></span> &nbsp; UPDATE</button>
                        </div>
                        <div class="card-title">Progress WO 3P, Netizen & PS Indihome Reguler</div>
                        <div class="card-category">TELKOM AKSES REGIONAL 3 JAWA BARAT</div>
                        <table class="mt-3">
                            <tbody>
                                <tr>
                                    <td width="100" style="font-weight: bold;">AREA</td>
                                    <td style="padding-left: 10px">BANDUNG</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold;">TANGGAL</td>
                                    <!-- <td>: RABU, 21 AGUSTUS 2019 JAM 17.00 WIB</td> -->
                                    <td>
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <div class="form-group">
                                                  <input type="date" name="tanggal" id="tanggal" class="form-control" required="" value="{{ Carbon\Carbon::parse($tanggal)->format('Y-m-d') }}" max="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                                                </div>
                                            </div>
                                            <div class="col-xs-2"><button class="btn btn-xs btn-danger" id="goToDateBtn" style="margin-top: 15px;"><span class="fa fa-search"></span></button></div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- <div class="chart-container" style="min-height: 375px"> -->
                        <form method="POST" action="/diagram" id="diagvarsForm">
                            @csrf()
                            <div class="chart" id="diagram" style="overflow-x: auto;"></div>
                        </form>
                        <!-- </div> -->
                        <table class="table mt-3 table-bordered" id="summaryTable">
                            <thead>
                                <tr>
                                    <th scope="col" rowspan="3" style="background: #5B9BD5; color: #fff">Datel</th>
                                    <th scope="col" colspan="2" style="background: #9900CC; color: #fff">TEKNISI</th>
                                    <th scope="col" colspan="7" style="background: #FFCC00">Total WO (2P dan 3P)</th>
                                    <th scope="col" rowspan="2" colspan="5" style="background: #FFFF00">Live</th>
                                    <th scope="col" rowspan="3" style="background: #FFFF00">Total</th>
                                    <th scope="col" rowspan="3" style="background: #5B9BD5; color: #fff">PROD %</th>
                                    <th scope="col" rowspan="3" style="background: #FFCC00">Rasio<br />PS/WO</th>
                                </tr>
                                <tr>
                                    <th scope="col" rowspan="2" style="background: #9900CC; color: #fff">GROUP</th>
                                    <th scope="col" rowspan="2" style="background: #9900CC; color: #fff">JML</th>
                                    @foreach($hours as $h)
                                    <th scope="col" @if ($h->khusus == 1) rowspan="2" @endif style="background: #FFCC00">@if ($h->khusus == 1) PAGI @else JAM @endif</th>
                                    @endforeach
                                    <th scope="col" rowspan="2" style="background: #FFCC00">TOT</th>
                                </tr>
                                <tr>
                                    @foreach($hours->where('khusus', 0) as $h)
                                    <th scope="col" style="background: #FFCC00">{{ $h->jam }}</th>
                                    @endforeach
                                    @foreach ($types as $t)
                                    <th scope="col" style="background: #FFFF00">{{ $t->id }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datel as $d)
                                <tr>
                                    <td scope="col" rowspan="2">{{ $d->nama }}</td>
                                    <td scope="col">TA</td>
                                    <td scope="col">{{ $teknisi_count[$d->id]['TA'] }}</td>
                                    @foreach($hours as $h)
                                    <td scope="col">{{ $order_by_hour[$d->id]['TA'][$h->jam] }}</td>
                                    @endforeach
                                    <td scope="col">{{ $order_2p3p_total[$d->id]['TA'] }}</td>
                                    @foreach($types as $t)
                                    <td scope="col">{{ $order_by_type[$d->id]['TA'][$t->id] }}</td>
                                    @endforeach
                                    <td scope="col">{{ $order_by_type_datel_total[$d->id]['TA'] }}</td>
                                    <td scope="col">{{ $teknisi_count[$d->id]['TA'] > 0 ? $order_by_type_datel_total[$d->id]['TA'] / $teknisi_count[$d->id]['TA'] : 0 }}</td>
                                    <td scope="col">{{ $order_2p3p_total[$d->id]['TA'] > 0 ? $order_by_type_datel_total[$d->id]['TA'] / $order_2p3p_total[$d->id]['TA'] * 100: 0 }}%</td>
                                </tr>
                                <tr>
                                    <td scope="col">PA</td>
                                    <td scope="col">{{ $teknisi_count[$d->id]['PA'] }}</td>
                                    @foreach($hours as $h)
                                    <td scope="col">{{ $order_by_hour[$d->id]['PA'][$h->jam] }}</td>
                                    @endforeach
                                    <td scope="col">{{ $order_2p3p_total[$d->id]['PA'] }}</td>
                                    @foreach($types as $t)
                                    <td scope="col">{{ $order_by_type[$d->id]['PA'][$t->id] }}</td>
                                    @endforeach
                                    <td scope="col">{{ $order_by_type_datel_total[$d->id]['PA'] }}</td>
                                    <td scope="col">{{ $teknisi_count[$d->id]['PA'] > 0 ? $order_by_type_datel_total[$d->id]['PA'] / $teknisi_count[$d->id]['PA'] : 0 }}</td>
                                    <td scope="col">{{ $order_2p3p_total[$d->id]['PA'] > 0 ? $order_by_type_datel_total[$d->id]['PA'] / $order_2p3p_total[$d->id]['PA'] * 100: 0 }}%</td>
                                </tr>
                                @endforeach

                                <!-- Footer -->
                                <tr>
                                    <td scope="col" colspan="2">Total</td>
                                    <td scope="col">{{ $total_teknisi }}</td>
                                    @foreach($hours as $h)
                                    <td scope="col">{{ $order_by_hour_total[$h->jam] }}</td>
                                    @endforeach
                                    <td scope="col">{{ array_sum($order_by_hour_total) }}</td>
                                    @foreach($types as $t)
                                    <td scope="col">{{ $order_by_type_total[$t->id] }}</td>
                                    @endforeach
                                    <td scope="col">{{ array_sum($order_by_type_total) }}</td>
                                    <td scope="col">{{ $total_teknisi > 0 ? round(array_sum($order_by_type_total) / $total_teknisi, 1) : 0 }}</td>
                                    <td scope="col">{{ array_sum($order_by_hour_total) > 0 ? round(array_sum($order_by_type_total) / array_sum($order_by_hour_total), 2) * 100 : 0 }}%</td>
                                </tr>
                                <!-- End Footer -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="/assets/vendor/treantjs/raphael.js"></script>
<script src="/assets/vendor/treantjs/Treant.js"></script>
<script src="/assets/vendor/daterangepicker/moment.min.js"></script>
<script>
    $(function() {
        var tree_structure = {
            chart: {
                container: "#diagram",
                levelSeparation:    50,
                siblingSeparation:  15,
                subTeeSeparation:   15,
                rootOrientation: "WEST",

                node: {
                    HTMLclass: "tennis-draw",
                    drawLineThrough: true
                },
                connectors: {
                    type: "straight",
                    style: {
                        "stroke-width": 2,
                        "stroke": "#ccc"
                    }
                }
            },
            
            nodeStructure: {
                innerHTML: "<table class='diagram-table wo'><tbody><tr><td width='60'>WO</td></tr><tr><td>{{ $orders->count() }}</td></tr></tbody></table>",
                children: [
                    {
                        innerHTML: "<table class='diagram-table p2msdn'><tbody><tr><td width='80' colspan='2'>P2 MSDN</td></tr><tr><td>{{ $diag_vars->ps_3p + $diag_vars->ps_2p }}</td><td>{{ $orders->count() > 0 ? (($diag_vars->ps_3p + $diag_vars->ps_2p) / $orders->count()) * 100 : 0 }}%</td></tr></tbody></table>",
                        children: [
                            {
                                innerHTML: "<table class='diagram-table thirdlevel p2msdn'><tbody><tr><td colspan='3'>3P</td></tr><tr><td class='editable'><input type='text' name='ps_3p' readonly value='{{ $diag_vars->ps_3p }}'></td><td></td><td>{{ $orders->count() > 0 ? ($diag_vars->ps_3p / $orders->count()) * 100 : 0 }}%</td></tr></tbody></table>",
                            },
                            {
                                innerHTML: "<table class='diagram-table thirdlevel p2msdn'><tbody><tr><td colspan='3'>2P</td></tr><tr><td class='editable'><input type='text' name='ps_2p' readonly value='{{ $diag_vars->ps_2p }}'></td><td></td><td>{{ $orders->count() > 0 ? ($diag_vars->ps_2p / $orders->count()) * 100 : 0 }}%</td></tr></tbody></table>",
                            },
                        ]
                    },
                    {
                        innerHTML: "<table class='diagram-table nonps'><tbody><tr><td width='80' colspan='2'>NON PS</td></tr><tr><td>{{ $orders->count() - ($diag_vars->ps_3p + $diag_vars->ps_2p) }}</td><td>{{ $orders->count() > 0 ? (($orders->count() - ($diag_vars->ps_3p + $diag_vars->ps_2p)) / $orders->count()) * 100 : 0 }}%</td></tr></tbody></table>",
                        children: [
                            {
                                innerHTML: "<table class='diagram-table thirdlevel kendala'><tbody><tr><td colspan='3'>KENDALA</td></tr><tr><td>{{ $orders->count() - ($order_by_type_total['3P'] + $order_by_type_total['2P'] + $order_by_type_total['UPSELLING'] - ($diag_vars->ps_3p + $diag_vars->ps_2p)) }}</td><td></td><td>{{ $orders->count() > 0 ? (($orders->count() - ($order_by_type_total['3P'] + $order_by_type_total['2P'] + $order_by_type_total['UPSELLING'] - ($diag_vars->ps_3p + $diag_vars->ps_2p))) / $orders->count()) * 100 : 0 }}%</td></tr></tbody></table>",
                                childrenDropLevel: 1,
                                connectors: {
                                    type: "straight",
                                    style: {
                                        "stroke-width": 5,
                                        "stroke": "#81B861",
                                        "arrow-end": "block-wide-long"
                                    }
                                },
                                children: [
                                    {
                                        innerHTML: "<table class='diagram-table description'>"+
                                            "<tbody>"+
                                                "<tr>"+
                                                    "<td>PS BLM MASUK MS2N</td>"+
                                                    "<td class='editable @if ($diag_vars->ps_non_ms2n > 0) exist @endif'><input type='text' name='ps_non_ms2n' value='{{ $diag_vars->ps_non_ms2n }}' readonly></td>"+
                                                    "<td class='@if ($diag_vars->ps_non_ms2n > 0) exist @endif'>{{ $orders->count() > 0 ? ($diag_vars->ps_non_ms2n / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "<tr>"+
                                                    "<td>LIVE RWOS</td>"+
                                                    "<td class='editable @if ($diag_vars->live_rwos > 0) exist @endif'><input type='text' name='live_rwos' value='{{ $diag_vars->live_rwos }}' readonly></td>"+
                                                    "<td class='@if ($diag_vars->live_rwos > 0) exist @endif'>{{ $orders->count() > 0 ? ($diag_vars->live_rwos / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "<tr>"+
                                                    "<td>LIVE ACTCOMP</td>"+
                                                    "<td class='editable @if ($diag_vars->live_actcomp > 0) exist @endif'><input type='text' name='live_actcomp' value='{{ $diag_vars->live_actcomp }}' readonly></td>"+
                                                    "<td class='@if ($diag_vars->live_actcomp > 0) exist @endif'>{{ $orders->count() > 0 ? ($diag_vars->live_actcomp / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "<tr>"+
                                                    "<td>LIVE FALLOUT</td>"+
                                                    "<td class='editable @if ($diag_vars->live_fallout > 0) exist @endif'><input type='text' name='live_fallout' value='{{ $diag_vars->live_fallout }}' readonly></td>"+
                                                    "<td class='@if ($diag_vars->live_fallout > 0) exist @endif'>{{ $orders->count() > 0 ? ($diag_vars->live_fallout / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "<tr>"+
                                                    "<td>FALLOUT</td>"+
                                                    "<td class='editable @if ($diag_vars->fallout > 0) exist @endif'><input type='text' name='fallout' value='{{ $diag_vars->fallout }}' readonly></td>"+
                                                    "<td class='@if ($diag_vars->fallout > 0) exist @endif'>{{ $orders->count() > 0 ? ($diag_vars->fallout / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "<tr>"+
                                                    "<td>TOTAL</td>"+
                                                    "<td>{{ ($diag_vars->ps_non_ms2n + $diag_vars->live_rwos + $diag_vars->live_actcomp + $diag_vars->live_fallout + $diag_vars->fallout) }}</td>"+
                                                    "<td>{{ $orders->count() > 0 ? (($diag_vars->ps_non_ms2n + $diag_vars->live_rwos + $diag_vars->live_actcomp + $diag_vars->live_fallout + $diag_vars->fallout) / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "</tbody>"+
                                            "</table>",
                                        childrenDropLevel: 1,
                                        connectors: {
                                            type: "straight",
                                            style: {
                                                "stroke-width": 5,
                                                "stroke": "#81B861",
                                                "arrow-end": "block-wide-long"
                                            }
                                        },
                                        children: [
                                            {
                                                innerHTML: "<table class='diagram-table fifthlevel fallout'>"+
                                                        "<tbody>"+
                                                            "<tr>"+
                                                                "<td>Fallout (Activation)</td>"+
                                                                "<td class='editable'><input type='text' name='fallout_act' value='{{ $diag_vars->fallout_act }}' readonly></td>"+
                                                            "</tr>"+
                                                            "<tr>"+
                                                                "<td>Fallout (Data)</td>"+
                                                                "<td class='editable'><input type='text' name='fallout_data' value='{{ $diag_vars->fallout_data }}' readonly></td>"+
                                                            "</tr>"+
                                                            "<tr>"+
                                                                "<td>Fallout (WFM)</td>"+
                                                                "<td class='editable'><input type='text' name='fallout_wfm' value='{{ $diag_vars->fallout_wfm }}' readonly></td>"+
                                                            "</tr>"+
                                                            "<tr>"+
                                                                "<td>Fallout (OSM)</td>"+
                                                                "<td class='editable'><input type='text' name='fallout_osm' value='{{ $diag_vars->fallout_osm }}' readonly></td>"+
                                                            "</tr>"+
                                                            "<tr>"+
                                                                "<td>Mandeg</td>"+
                                                                "<td class='editable'><input type='text' name='mandeg' value='{{ $diag_vars->mandeg }}' readonly></td>"+
                                                            "</tr>"+
                                                        "</tbody>"+
                                                    "</table>"+
                                                    "<table class='diagram-table fifthlevel'>"+
                                                        "<tbody>"+
                                                            "<tr>"+
                                                                "<td>WO PAGI</td>"+
                                                                "<td>{{ $order_by_hour_total[8] }}</td>"+
                                                            "</tr>"+
                                                            "<tr>"+
                                                                "<td>WO INPUT HARI INI</td>"+
                                                                "<td>{{ array_sum($order_by_hour_total) - $order_by_hour_total[8] }}</td>"+
                                                            "</tr>"+
                                                            "<tr>"+
                                                                "<td>JUMLAH WO</td>"+
                                                                "<td>{{ array_sum($order_by_hour_total) }}</td>"+
                                                            "</tr>"+
                                                        "</tbody>"+
                                                    "</table>",
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                innerHTML: "<table class='diagram-table thirdlevel progresswo'><tbody><tr><td colspan='3'>PROGRESS WO</td></tr><tr><td>{{ $order_by_type_total['3P'] + $order_by_type_total['2P'] + $order_by_type_total['UPSELLING'] - ($diag_vars->ps_3p + $diag_vars->ps_2p) }}</td><td></td><td>{{ $orders->count() > 0 ? (($order_by_type_total['3P'] + $order_by_type_total['2P'] + $order_by_type_total['UPSELLING'] - ($diag_vars->ps_3p + $diag_vars->ps_2p)) / $orders->count()) * 100 : 0 }}%</td></tr></tbody></table>",
                                childrenDropLevel: 1,
                                connectors: {
                                    type: "straight",
                                    style: {
                                        "stroke-width": 5,
                                        "stroke": "#F18C55",
                                        "arrow-end": "block-wide-long"
                                    }
                                },
                                children: [
                                    {
                                        innerHTML: "<table class='diagram-table description2'>"+
                                            "<tbody>"+
                                                @php ($total_prog = 0)
                                                @foreach ($progress_wo as $p)
                                                "<tr>"+
                                                    "<td>{{ strtoupper($p->deskripsi) }}</td>"+
                                                    "<td>{{ $p->work_order_count }}</td>"+
                                                    "<td>{{ $orders->count() > 0 ? ($p->work_order_count / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                @php ($total_prog += $p->work_order_count)
                                                @endforeach
                                                "<tr>"+
                                                    "<td>SISA WO (onprogress)</td>"+
                                                    "<td>{{ $sisa_wo->sum('work_order_count') }}</td>"+
                                                    "<td>{{ $orders->count() > 0 ? ($sisa_wo->sum('work_order_count') / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "<tr>"+
                                                    "<td>TOTAL</td>"+
                                                    "<td>{{ $total_prog + $sisa_wo->sum('work_order_count') }}</td>"+
                                                    "<td>{{ $orders->count() > 0 ? (($total_prog + $sisa_wo->sum('work_order_count')) / $orders->count()) * 100 : 0 }}%</td>"+
                                                "</tr>"+
                                                "</tbody>"+
                                            "</table>",
                                        childrenDropLevel: 1,
                                        connectors: {
                                            type: "straight",
                                            style: {
                                                "stroke-width": 5,
                                                "stroke": "#81B861",
                                                "arrow-end": "block-wide-long"
                                            }
                                        },
                                        children: [
                                            {
                                                innerHTML: "<table class='diagram-table sc-table'>"+
                                                        "<tbody>"+
                                                            "<tr>"+
                                                                "<td>Row Labels</td>"+
                                                                "<td>Count of SC</td>"+
                                                            "</tr>"+
                                                            @php ($total_s = 0)
                                                            @foreach ($progress_status as $s)
                                                            "<tr class='parent'>"+
                                                                "<td>{{ $s->deskripsi }}</td>"+
                                                                "<td>{{ $s->ada_kendala($tanggal)->sum('work_order_count') }}</td>"+
                                                            "</tr>"+
                                                            @foreach ($s->ada_kendala($tanggal) as $k)
                                                            "<tr class='child'>"+
                                                                "<td>{{ $k->deskripsi }}</td>"+
                                                                "<td>{{ $k->wo_count($tanggal) }}</td>"+
                                                            "</tr>"+
                                                            @php ($total_s += $s->ada_kendala($tanggal)->sum('work_order_count'))
                                                            @endforeach
                                                            @endforeach
                                                            "<tr class='total'>"+
                                                                "<td>Grand Total</td>"+
                                                                "<td>{{ $total_s }}</td>"+
                                                            "</tr>"+
                                                        "</tbody>"+
                                                    "</table>",
                                            }
                                        ]
                                    }
                                ]
                            },
                        ]
                    },
                ]
            }
        };

        new Treant( tree_structure );

        $('body').on('click', '.editBtn', function() {
            $('.editable input').prop('readonly', false);
            $(this).html('<span class="fa fa-save"></span> &nbsp; SIMPAN');
            $(this).addClass('saveBtn btn-primary');
            $(this).removeClass('editBtn btn-danger');
        });

        $('body').on('click', '.saveBtn', function() {
            $('#diagvarsForm').submit();
        });

        $('#goToDateBtn').click(function() {
            window.location.href='/?tanggal='+$('#tanggal').val();
        });
    });
</script>
@endsection