<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'HomeController@index')->name('home');


	Route::group(['middleware' => 'is_admin'], function() {
		Route::get('/teknisi/dtIndex', 'TeknisiController@dtIndex')->name('teknisi.dtIndex');
		Route::resource('/teknisi', 'TeknisiController')->names([
			'index' => 'teknisi.index',
		]);

		Route::get('/user/dtIndex', 'UserController@dtIndex')->name('user.dtIndex');
		Route::resource('/user', 'UserController')->names([
			'index' => 'user.index',
		]);
	});

	Route::get('/work_order/dtIndex', 'WorkOrderController@dtIndex')->name('order.dtIndex');
	Route::resource('/work_order', 'WorkOrderController')->names([
		'index' => 'order.index',
		'create' => 'order.create'
	]);

	Route::get('/teknisi_harian/dtIndex', 'TeknisiHarianController@dtIndex')->name('tek_harian.dtIndex');
	Route::resource('/teknisi_harian', 'TeknisiHarianController')->names([
		'index' => 'tek_harian.index',
	]);

	Route::post('/diagram', 'HomeController@diagvars_update');
});
