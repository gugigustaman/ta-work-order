<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeknisiHarian extends Model
{
    protected $table = 'teknisi_harian';

    public function reporter() {
    	return $this->belongsTo('App\User', 'reporter_id', 'id');
    }

    public function sto() {
    	return $this->belongsTo('App\Sto', 'sto_id', 'id');
    }

    public static function getTeknisiCount($datel_id, $group, $date_start = null, $date_end = null) {
    	$g = strtolower($group);

    	$th = self::whereHas('sto', function($q) use ($datel_id) {
    		$q->where('datel_id', $datel_id);
    	})->whereDate('tanggal', $date_start)->first();

    	if (!$th) return 0;

    	return $th->{$g};
    }
}
