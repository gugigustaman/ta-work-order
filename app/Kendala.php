<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Kendala extends Model
{
    protected $table = 'kendala';

    public $incrementing = false;

    protected $appends = ['last_count_today'];

    public function work_order() {
    	return $this->hasMany('App\WorkOrder', 'kendala', 'id');
    }

    public function wo_count($date) {
    	if ($date == null) {
    		$date = Carbon::now();
    	}  	
    	
    	return $this->work_order()
    		->whereDate('created_at', $date->format('Y-m-d'))
    		->count();
    }

    public function getLastCountTodayAttribute() {
    	return $this->work_order()
    		->whereDate('created_at', Carbon::now()->format('Y-m-d'))
    		->count();
    }
}
