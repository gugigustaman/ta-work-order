<?php

namespace App;

class UserLevel
{
    public static $levels = [
    	[
    		'id' => 1,
    		'nama' => 'Administrator',
    	],
    	[
    		'id' => 2,
    		'nama' => 'Team Leader',
    	],
    	[
    		'id' => 3,
    		'nama' => 'Help Desk',
    	],
    	[
    		'id' => 4,
    		'nama' => 'Teknisi TA',
    	]
    ];
}
