<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sto extends Model
{
    protected $table = 'sto';

    public $incrementing = false;
}
