<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sto;
use App\TeknisiHarianLevel;
use App\TeknisiHarian;
use Validator;
use DB;
use Auth;

class TeknisiHarianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teknisi_harian.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sto = Sto::all();
        return view('teknisi_harian.create', [
            'sto' => $sto
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required|date',
            'ta' => 'required|numeric|min:0',
            'pa' => 'required|numeric|min:0',
            'sto' => 'required|string|exists:sto,id',
        ]);

        if ($validator->fails()) {
            return redirect('/teknisi_harian/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            $teknisi_harian = new TeknisiHarian();
            $teknisi_harian->tanggal = $request->tanggal;
            $teknisi_harian->ta = $request->ta;
            $teknisi_harian->pa = $request->pa;
            $teknisi_harian->sto_id = $request->sto;
            $teknisi_harian->reporter_id = Auth::user()->id;
            $teknisi_harian->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/teknisi_harian/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/teknisi_harian')
            ->with('type', 'success')
            ->with('message', 'Berhasil menambahkan data teknisi harian.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teknisi_harian = TeknisiHarian::findOrFail($id);
        $sto = Sto::all();

        return view('teknisi_harian.edit', [
            'teknisi_harian' => $teknisi_harian,
            'sto' => $sto
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teknisi_harian = TeknisiHarian::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'tanggal' => 'required|date',
            'ta' => 'required|numeric|min:0',
            'pa' => 'required|numeric|min:0',
            'sto' => 'required|string|exists:sto,id',
        ]);

        if ($validator->fails()) {
            return redirect('/teknisi_harian/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {

            $teknisi_harian->tanggal = $request->tanggal;
            $teknisi_harian->ta = $request->ta;
            $teknisi_harian->pa = $request->pa;
            $teknisi_harian->sto_id = $request->sto;
            $teknisi_harian->reporter_id = Auth::user()->id;
            
            $teknisi_harian->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/teknisi_harian/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/teknisi_harian')
            ->with('type', 'success')
            ->with('message', 'Berhasil mengubah data teknisi harian.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teknisi_harian = TeknisiHarian::findOrFail($id);

        DB::beginTransaction();

        try {
            $teknisi_harian->delete();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/teknisi_harian')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/teknisi_harian')
            ->with('type', 'success')
            ->with('message', 'Berhasil menghapus data teknisi harian.');
    }

    public function dtIndex(Request $request) {
        $query = TeknisiHarian::with('reporter', 'sto');
        return datatables()->of($query)
            ->addIndexColumn()
            ->toJson();
    }
}
