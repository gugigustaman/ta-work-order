<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\Teknisi;
use Validator;
use DB;

class TeknisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teknisi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::all();

        return view('teknisi.create', [
            'areas' => $areas,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'grup' => 'required|string|in:PA,TA',
            'kode' => 'required|string|unique:teknisi,kode',
            'nama' => 'required|string',
            'area' => 'required|exists:area,id',
        ]);

        if ($validator->fails()) {
            return redirect('/teknisi/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            $teknisi = new Teknisi();

            $teknisi->grup = $request->grup;
            $teknisi->kode = $request->kode;
            $teknisi->nama = $request->nama;
            $teknisi->area_id = $request->area;
            
            $teknisi->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/teknisi/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/teknisi')
            ->with('type', 'success')
            ->with('message', 'Berhasil menambahkan teknisi.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $areas = Area::all();

        return view('teknisi.edit', [
            'areas' => $areas,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teknisi = Teknisi::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|exists:teknisi,id',
            'grup' => 'required|string|in:PA,TA',
            'kode' => 'required|string|unique:teknisi,kode,'.$id,
            'nama' => 'required|string',
            'area' => 'required|exists:area,id',
        ]);

        if ($validator->fails()) {
            return redirect('/teknisi/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            $teknisi = Teknisi::findOrFail($id);

            $teknisi->grup = $request->grup;
            $teknisi->kode = $request->kode;
            $teknisi->nama = $request->nama;
            $teknisi->area_id = $request->area;
            
            $teknisi->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/teknisi/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/teknisi')
            ->with('type', 'success')
            ->with('message', 'Berhasil mengubah teknisi.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teknisi = Teknisi::findOrFail($id);

        DB::beginTransaction();

        try {
            $teknisi->delete();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/teknisi')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/teknisi')
            ->with('type', 'success')
            ->with('message', 'Berhasil menghapus teknisi.');
    }

    public function dtIndex(Request $request) {
        $query = Teknisi::query();
        return datatables()->of($query)
            ->addIndexColumn()
            ->toJson();
    }
}
