<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datel;
use App\WorkOrder;
use App\JamWorkOrder;
use App\JenisWorkOrder;
use App\StatusWorkOrder;
use App\TeknisiHarian;
use App\VarDiagram;
use Carbon\Carbon;
use Validator;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $end = Carbon::now();

        if ($request->query('tanggal')) {
            $end = Carbon::parse($request->query('tanggal'));
        }

        $start = $end->copy()->setTime(0, 0, 0);

        $datel = Datel::all();
        $types = JenisWorkOrder::all();

        $hours = JamWorkOrder::all();

        $groups = ['TA', 'PA'];

        $orders = WorkOrder::getToday($start, $end);

        $teknisi_count = [];
        $total_teknisi = 0;

        $order_by_hour = [];
        $order_by_hour_total = [];
        $order_2p3p_total = [];

        $order_by_type = [];
        $order_by_type_total = [];
        $order_by_type_datel_total = [];

        foreach ($datel as $d) {
            $order_2p3p_total[$d->id] = [];
            $order_by_type_datel_total[$d->id] = [];

            foreach ($groups as $g) {
                $order_2p3p_total[$d->id][$g] = 0;
                $order_by_type_datel_total[$d->id][$g] = 0;

                $teknisi_count[$d->id][$g] = TeknisiHarian::getTeknisiCount($d->id, $g, $start, $end);

                $total_teknisi += $teknisi_count[$d->id][$g];

                foreach ($hours as $h) {
                    $order_by_hour[$d->id][$g][$h->jam] = WorkOrder::getOrderByHour($d->id, $g, $h->jam, $start, $end);

                    $order_2p3p_total[$d->id][$g] += $order_by_hour[$d->id][$g][$h->jam];

                    if (isset($order_by_hour_total[$h->jam])) {
                        $order_by_hour_total[$h->jam] += $order_by_hour[$d->id][$g][$h->jam];
                    } else {
                        $order_by_hour_total[$h->jam] = $order_by_hour[$d->id][$g][$h->jam];
                    }
                }

                foreach ($types as $t) {
                    $order_by_type[$d->id][$g][$t->id] = WorkOrder::getOrderByType($d->id, $g, $t->id, $start, $end);

                    $order_by_type_datel_total[$d->id][$g] += $order_by_type[$d->id][$g][$t->id];

                    if (isset($order_by_type_total[$t->id])) {
                        $order_by_type_total[$t->id] += $order_by_type[$d->id][$g][$t->id];
                    } else {
                        $order_by_type_total[$t->id] = $order_by_type[$d->id][$g][$t->id];
                    }
                }
            }
        }

        $progress_wo = StatusWorkOrder::withOtherDayCount($end);

        $sisa_wo = StatusWorkOrder::withSisaOtherDayCount($end);

        $progress_status = StatusWorkOrder::whereIn('id', [
            'KENDALA TEKNIS',
            'KENDALA PELANGGAN',
            'DOUBLE INPUT',
        ])->get();

        $diag_vars = VarDiagram::whereDate('created_at', $end)->orderBy('id', 'desc')->first();

        if (!$diag_vars) {
            $diag_vars = new VarDiagram();
            $diag_vars->ps_3p = 0;
            $diag_vars->ps_2p = 0;
            $diag_vars->ps_non_ms2n = 0;
            $diag_vars->live_rwos = 0;
            $diag_vars->live_actcomp = 0;
            $diag_vars->live_fallout = 0;
            $diag_vars->fallout = 0;
            $diag_vars->fallout_act = 0;
            $diag_vars->fallout_data = 0;
            $diag_vars->fallout_wfm = 0;
            $diag_vars->fallout_osm = 0;
            $diag_vars->mandeg = 0;
        }

        return view('home', [
            'tanggal' => $end,
            'datel' => $datel,
            'types' => $types,
            'hours' => $hours,
            'groups' => $groups,
            'orders' => $orders,
            'teknisi_count' => $teknisi_count,
            'total_teknisi' => $total_teknisi,
            'order_by_hour' => $order_by_hour,
            'order_by_hour_total' => $order_by_hour_total,
            'order_2p3p_total' => $order_2p3p_total,
            'order_by_type' => $order_by_type,
            'order_by_type_total' => $order_by_type_total,
            'order_by_type_datel_total' => $order_by_type_datel_total,
            'progress_wo' => $progress_wo,
            'sisa_wo' => $sisa_wo,
            'progress_status' => $progress_status,
            'diag_vars' => $diag_vars
        ]);
    }

    public function diagvars_update(Request $request) {
        $validation = Validator::make($request->all(), [
            'ps_3p' => 'required|numeric|min:0',
            'ps_2p' => 'required|numeric|min:0',
            'ps_non_ms2n' => 'required|numeric|min:0',
            'live_rwos' => 'required|numeric|min:0',
            'live_actcomp' => 'required|numeric|min:0',
            'live_fallout' => 'required|numeric|min:0',
            'fallout' => 'required|numeric|min:0',
            'fallout_act' => 'required|numeric|min:0',
            'fallout_data' => 'required|numeric|min:0',
            'fallout_wfm' => 'required|numeric|min:0',
            'fallout_osm' => 'required|numeric|min:0',
            'mandeg' => 'required|numeric|min:0',
        ]);

        if ($validation->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validation->errors)
                ->with('type', 'danger')
                ->with('message', 'Mohon lengkapi kolom-kolom pada diagram terlebih dahulu.');
        }

        DB::beginTransaction();
        try {

            $vars = new VarDiagram();
            $vars->ps_3p = $request->ps_3p;
            $vars->ps_2p = $request->ps_2p;
            $vars->ps_non_ms2n = $request->ps_non_ms2n;
            $vars->live_rwos = $request->live_rwos;
            $vars->live_actcomp = $request->live_actcomp;
            $vars->live_fallout = $request->live_fallout;
            $vars->fallout = $request->fallout;
            $vars->fallout_act = $request->fallout_act;
            $vars->fallout_data = $request->fallout_data;
            $vars->fallout_wfm = $request->fallout_wfm;
            $vars->fallout_osm = $request->fallout_osm;
            $vars->mandeg = $request->mandeg;
            $vars->reporter_id = Auth::user()->id;

            $vars->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return redirect('/')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem.');
        }

        return redirect('/')
            ->withInput()
            ->with('type', 'success')
            ->with('message', 'Berhasil update diagram.');

    }
}
