<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkOrder;
use App\WorkOrderLog;
use Validator;
use DB;
use \Exception;
use Auth;
use App\Teknisi;

class WorkOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orders' => 'required|string'
        ]);

        if ($validator->fails()) {
            return redirect('/work_order/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        $orders = explode("\r\n", $request->orders);

        if (count($orders) == 0) {
            return redirect('/work_order/create')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            foreach ($orders as $o) {
                $oR = explode(';', $o);
                
                if (count($oR) < 9) {
                    throw new Exception("Error Processing Request", 15);
                }

                $wo_validator = Validator::make($oR, [
                    0 => 'required|numeric|exists:jam_work_order,jam',
                    1 => 'required|string|exists:sto,id',
                    2 => 'required|string|exists:jenis_work_order,id',
                    3 => 'required|string',
                    4 => 'required|string',
                    5 => 'required|string',
                    6 => 'required|string|exists:status_work_order,id',
                    7 => 'nullable|string',
                    8 => 'nullable|string',
                ], [
                    0 => 'Jam input tidak valid.',
                    1 => 'STO tidak ditemukan.',
                    6 => 'Status tidak valid.',
                    'required' => 'Work order tidak valid'
                ]);

                if ($wo_validator->fails()) {
                    throw new Exception($wo_validator->errors(), 15);
                }

                $work_order = WorkOrder::findBySc($oR[3]);

                if (!$work_order) {
                    $work_order = new WorkOrder();
                }

                $work_order->jenis_work_order_id = $oR[2];
                $work_order->sto_id = $oR[1];
                $work_order->no_speedy = $oR[4];
                $work_order->no_sc = $oR[3];
                $work_order->jam = $oR[0];
                $work_order->status = $oR[6];
                $work_order->kendala = $oR[7] != '' ? $oR[7] : null;
                $work_order->keterangan = $oR[8];
                $work_order->reporter_id = Auth::user()->id;
                
                $work_order->save();

                $techs = explode(',', $oR[5]);

                $ts = [];

                foreach ($techs as $tech) {
                    $t = Teknisi::where('kode', $tech)->first();

                    if ($t) {
                        $ts[] = $t->id;
                    } else {
                        throw new Exception('Teknisi tidak ditemukan.', 15);
                    }
                }

                $work_order->teknisi()->sync($ts);

                $log = new WorkOrderLog();
                $log->work_order_id = $work_order->id;
                $log->jam = $oR[0];
                $log->status = $oR[6];
                $log->kendala = $oR[7] != '' ? $oR[7] : null;
                $log->keterangan = $oR[8];
                $log->reporter_id = Auth::user()->id;
                $log->save();
            }

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            if ($e->getCode() == 15) {
                return redirect('/work_order/create')
                    ->withInput()
                    ->with('type', 'danger')
                    ->with('message', $e->getMessage());
            }

            return redirect('/work_order/create')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        return redirect('/work_order')
            ->with('type', 'success')
            ->with('message', 'Berhasil menambahkan order.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $work_order = WorkOrder::findOrFail($id);
        $areagroups = AreaGroup::all();
        $work_orderlevels = WorkOrderLevel::$levels;

        return view('order.edit', [
            'order' => $work_order,
            'areagroups' => $areagroups,
            'orderlevels' => $work_orderlevels,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $work_order = WorkOrder::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'nama' => 'required|string',
            'ordername' => 'required|string|unique:orders,ordername,'.$id,
            'area' => 'required|exists:area_groups,id',
            'level' => 'required|in:1,2,3,4',
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/work_order/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            $work_order->ordername = $request->ordername;
            $work_order->name = $request->nama;
            $work_order->area_group_id = $request->area;
            $work_order->level = $request->level;

            if ($request->password) {
                $work_order->password = bcrypt($request->password);    
            }
            
            $work_order->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/work_order/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/work_order')
            ->with('type', 'success')
            ->with('message', 'Berhasil mengubah order.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $work_order = WorkOrder::findOrFail($id);

        DB::beginTransaction();

        try {
            $work_order->delete();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/work_order')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/work_order')
            ->with('type', 'success')
            ->with('message', 'Berhasil menghapus order.');
    }

    public function dtIndex(Request $request) {
        $query = WorkOrder::with('reporter');
        return datatables()->of($query)
            ->addIndexColumn()
            ->toJson();
    }
}
