<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\UserLevel;
use App\User;
use Validator;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::all();
        $userlevels = UserLevel::$levels;

        return view('user.create', [
            'areas' => $areas,
            'userlevels' => $userlevels,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string',
            'username' => 'required|string|unique:user,id',
            'area' => 'required|exists:area,id',
            'level' => 'required|in:1,2,3,4',
            'password' => 'required|string|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/user/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            $user = new User();
            $user->id = $request->username;
            $user->nama = $request->nama;
            $user->area_id = $request->area;
            $user->level = $request->level;
            $user->password = bcrypt($request->password);
            $user->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/user/create')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/user')
            ->with('type', 'success')
            ->with('message', 'Berhasil menambahkan user.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $areagroups = AreaGroup::all();
        $userlevels = UserLevel::$levels;

        return view('user.edit', [
            'user' => $user,
            'areagroups' => $areagroups,
            'userlevels' => $userlevels,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'nama' => 'required|string',
            'username' => 'required|string|unique:user,id,'.$id,
            'area' => 'required|exists:area,id',
            'level' => 'required|in:1,2,3,4',
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/user/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Silakan lengkapi terlebih dahulu formulir berikut dengan benar.');
        }

        DB::beginTransaction();

        try {
            $user->id = $request->username;
            $user->nama = $request->nama;
            $user->area_id = $request->area;
            $user->level = $request->level;

            if ($request->password) {
                $user->password = bcrypt($request->password);    
            }
            
            $user->save();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/user/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/user')
            ->with('type', 'success')
            ->with('message', 'Berhasil mengubah user.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        DB::beginTransaction();

        try {
            $user->delete();

            DB::commit();   
        } catch (Exception $e) {
            DB::rollback();

            return redirect('/user')
                ->withInput()
                ->with('type', 'danger')
                ->with('message', 'Terjadi kesalahan pada sistem. Silakan ulangi beberapa saat lagi.');
        }

        return redirect('/user')
            ->with('type', 'success')
            ->with('message', 'Berhasil menghapus user.');
    }

    public function dtIndex(Request $request) {
        $query = User::query();
        return datatables()->of($query)
            ->addIndexColumn()
            ->toJson();
    }
}
