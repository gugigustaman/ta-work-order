<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class StatusWorkOrder extends Model
{
    protected $table = 'status_work_order';

    public $incrementing = false;

    public function work_order() {
    	return $this->hasMany('App\WorkOrder', 'status', 'id');
    }

    public function wo_count($date) {
    	return $this->work_order()->whereDate('created_at', $date->format('Y-m-d'))->count();
    }

    public static function withOtherDayCount($date = null) {
    	if ($date == null) {
    		$date = Carbon::now();
    	}

    	$progress_wo = StatusWorkOrder::whereIn('id', [
    	    'TARIK DC',
    	    'KENDALA TEKNIS',
    	    'KENDALA PELANGGAN',
    	    'DOUBLE INPUT',
    	])->orderBy('id', 'DESC')->get();

    	foreach ($progress_wo as $w) {
    		$w->work_order_count = $w->wo_count($date);
    	}

    	return $progress_wo;
    }

    public static function withSisaOtherDayCount($date = null) {
    	if ($date == null) {
    		$date = Carbon::now();
    	}

    	$progress_wo = StatusWorkOrder::whereIn('id', [
    	    'KENDALA TEKNIS',
            'KENDALA PELANGGAN',
            'DOUBLE INPUT',
            'TARIK DC',
            'LIVE', 
            'PS'
    	])->orderBy('id', 'DESC')->get();

    	foreach ($progress_wo as $w) {
    		$w->work_order_count = $w->wo_count($date);
    	}

    	return $progress_wo;
    }

    public function kendala() {
    	return $this->hasMany('App\Kendala', 'status_work_order_id', 'id');
    }

    public function ada_kendala($date) {
    	if ($date == null) {
    		$date = Carbon::now();
    	}

    	foreach ($this->kendala as $k) {
    		$k->work_order_count = $k->wo_count($date);
    	}

    	return $this->kendala->where('work_order_count', '>', 0);
    }
    
}
