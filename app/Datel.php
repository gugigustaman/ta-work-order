<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datel extends Model
{
    protected $table = 'datel';

    public $incrementing = false;
}
