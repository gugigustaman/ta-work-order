<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisWorkOrder extends Model
{
    protected $table = 'jenis_work_order';

    public $incrementing = false;
}
