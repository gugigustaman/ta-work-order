<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WorkOrderLog extends Model
{
    protected $table = 'work_order_log';

    public function reporter() {
    	return $this->belongsTo('App\User', 'reporter_id', 'id');
    }

	public function sto() {
		return $this->belongsTo('App\Sto', 'sto_id', 'id');
	}    

    public function teknisi() {
    	return $this->belongsToMany('App\Teknisi', 'teknisi_work_order', 'work_order_id', 'teknisi_id')->withTimestamps();
    }

    public static function findBySc($sc) {
        return self::where('no_sc', $sc)->first();
    }

    public static function getToday($date_start = null, $date_end = null) {
    	if ($date_start == null || $date_end == null) {
    		$date_end = Carbon::now();
    		$date_start = $date_end->copy()->setTime(0, 0, 0);
    	}

	    $orders = WorkOrder::whereBetween('created_at', [$date_start, $date_end])
	    	->get();
    	
    	return $orders;
    }

    public static function getTeknisiCount($datel_id, $group, $date_start = null, $date_end = null) {
    	if ($date_start == null || $date_end == null) {
    		$date_end = Carbon::now();
    		$date_start = $date_end->copy()->setTime(0, 0, 0);
    	}

	    $orders = WorkOrder::withCount('teknisi')->whereHas('sto', function($q) use ($datel_id) {
	    		$q->where('datel_id', $datel_id);
	    	})->whereHas('teknisi', function($q) use ($group) {
	    		$q->where('grup', $group);
	    	})->whereBetween('created_at', [$date_start, $date_end])
	    	->get();
    	
    	return $orders->sum('teknisi_count');
    }

    public static function getOrderByHour($datel_id, $group, $hour, $date_start = null, $date_end = null) {
    	if ($date_start == null || $date_end == null) {
    		$date_end = Carbon::now();
    		$date_start = $date_end->copy()->setTime(0, 0, 0);
    	}

	    $orders = WorkOrder::withCount('teknisi')->whereHas('sto', function($q) use ($datel_id) {
	    		$q->where('datel_id', $datel_id);
	    	})->whereHas('teknisi', function($q) use ($group) {
	    		$q->where('grup', $group);
	    	})->whereBetween('created_at', [$date_start, $date_end])
	    	->where('jam', $hour)
	    	->whereIn('jenis_work_order_id', ['2P', '3P'])
	    	->count();
    	
    	return $orders;
    }

        public static function getOrderByType($datel_id, $group, $type, $date_start = null, $date_end = null) {
        	if ($date_start == null || $date_end == null) {
        		$date_end = Carbon::now();
        		$date_start = $date_end->copy()->setTime(0, 0, 0);
        	}

    	    $orders = WorkOrder::withCount('teknisi')->whereHas('sto', function($q) use ($datel_id) {
    	    		$q->where('datel_id', $datel_id);
    	    	})->whereHas('teknisi', function($q) use ($group) {
    	    		$q->where('grup', $group);
    	    	})->whereBetween('created_at', [$date_start, $date_end])
    	    	->where('jenis_work_order_id', $type)
    	    	->count();
        	
        	return $orders;
        }

}
