<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JamWorkOrder extends Model
{
    protected $table = 'jam_work_order';
}
